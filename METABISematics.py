#!/usr/bin/env python
# coding: utf-8

# In[2]:


pip install pandas


# In[3]:


pip install tensorflow


# In[4]:


pip install nltk


# In[5]:


import pandas as pd
import numpy as np
from numpy.random import seed
import matplotlib.pyplot as plt
from matplotlib import style


# In[8]:


import tensorflow as tf


# In[9]:


import nltk


# In[10]:


foodchoice_df = pd.read_csv('foodchoicesample.csv')
foodchoice_df.head()


# In[11]:


foodchoice_df.info()


# In[13]:


foodchoice_df.isnull().sum()


# In[16]:


foodchoice_df['type_sports'] = foodchoice_df['type_sports'].astype('str')


# In[22]:


foodchoice_df['type_sports']


# In[24]:


foodchoice_df['type_sports'] = foodchoice_df['type_sports'].str.lower()


# In[26]:


foodchoice_df['type_sports']


# In[28]:


foodchoice_df['food_childhood'] = foodchoice_df['food_childhood'].astype('str')


# In[30]:


foodchoice_df['food_childhood']


# In[32]:


foodchoice_df['food_childhood'] = foodchoice_df['food_childhood'].str.lower()
foodchoice_df['food_childhood']


# In[34]:


#getting rid of targeted charachters in the trascription
chars = ['#',':,',': ,',';','$','!','?','*','``','1. ', '2. ', '3. ', '4. ', '5. ','6. ','7. ','8. ','9. ','10. ']
for c in chars:
    foodchoice_df['food_childhood'] = foodchoice_df['food_childhood'] = foodchoice_df['food_childhood'].str.replace(c,"")

foodchoice_df.sample(10)

# chars = "\`*_{}[]()>#+-.,!$:;%'&/?"


# In[36]:


# Tokenizing
import nltk
nltk.download('punkt')

from nltk.tokenize import sent_tokenize, word_tokenize

foodchoice_df['tokenized_sents'] = foodchoice_df['food_childhood'].apply(nltk.word_tokenize)
foodchoice_df.sample(5)


# In[38]:


import nltk
nltk.download('stopwords')

from nltk.corpus import stopwords
stop = stopwords.words('english')

foodchoice_df['post_stopwords'] = foodchoice_df['tokenized_sents'].apply(lambda x: [item for item in x if item not in stop])
foodchoice_df.sample(5)


# In[40]:


foodchoice_df['tokenized_sents'] = foodchoice_df['tokenized_sents'].astype('str')
foodchoice_df['tokenized_sents']


# In[42]:


foodchoice_df['tokens'] = foodchoice_df['tokenized_sents'].str.split().str.len()
foodchoice_df.sample(5)


# In[44]:


foodchoice_df['fav_cuisine'].unique()


# In[46]:


#disproportion of corpora
foodchoice_df['fav_cuisine'].value_counts()


# In[48]:


foodchoice_df['fav_cuisine'].nunique()


# In[50]:


#Getting the mean and median across all tokens 
print(foodchoice_df['tokens'].mean())
print(foodchoice_df['tokens'].median())


# In[52]:


foodchoice_df['tokens'].value_counts().sample(10)


# In[54]:


foodchoice_df['fav_cuisine'].value_counts()


# In[56]:


foodchoice_df.groupby('fav_cuisine')['tokens'].agg(['count', 'mean', 'median']).sort_values(by='count',ascending = False)


# In[58]:


foodchoice_df.groupby('fav_cuisine')['tokens'].agg(['count', 'mean', 'median']).sort_values(by='count',ascending = False).plot(kind='bar', figsize=(15,7))
plt.ylabel("Food Choice Tokens number" )
plt.xlabel("fav_cuisine")
plt.grid()
plt.xticks(rotation = 90)
plt.legend(fancybox= True)
plt.savefig('Food_Choice_Corpus_view_with_tokens_number_WITHOUTREDUCTION_Beginning.png')


# In[59]:


import pandas as pd

foodchoice_df['weight'] = pd.to_numeric(foodchoice_df['weight'], errors='coerce')


mean = foodchoice_df['weight'].mean()
print(mean)


# In[62]:


median = foodchoice_df['weight'].median()
print(median)


# In[64]:


hist = foodchoice_df['weight'].hist(bins=10)


# In[66]:


import pandas as pd


foodchoice_df = pd.read_csv('foodchoicesample.csv')

foodchoice_df = foodchoice_df.dropna(subset=['weight', 'calories_scone'])

                                     
foodchoice_df.plot(kind='scatter', x='weight', y='calories_scone')


foodchoice_df.plot.bar(x='weight', y='calories_scone')


# In[67]:


import pandas as pd
import sklearn as sk
from sklearn.linear_model import LogisticRegression


foodsentiment_df = pd.read_csv('foodchoicesample.csv')

print(foodsentiment_df.columns)


if 'waffle_calories' in foodsentiment_df.columns:
    
    X = foodsentiment_df[['calories_scone', 'waffle_calories']].dropna()
    y = foodsentiment_df['Gender'].loc[X.index]
    
else:
    print("Column 'waffle_calories' not found.  Please check the column names.")

model = LogisticRegression()
if 'waffle_calories' in foodsentiment_df.columns:
    model.fit(X, y)
    predictions = model.predict(X)
    y_pred = model.predict(X)
    print(y_pred [:4])


# In[68]:


import pandas as pd
import sklearn as sk
from sklearn.linear_model import LogisticRegression


foodsentiment_df = pd.read_csv('foodchoicesample.csv')

print(foodsentiment_df.columns)


if 'waffle_calories' in foodsentiment_df.columns:
    
    X = foodsentiment_df[['calories_scone', 'waffle_calories']].dropna()
    y = foodsentiment_df['marital_status'].loc[X.index].dropna()


    X = X.loc[y.index]
    
else:
    print("Column 'waffle_calories' not found.  Please check the column names.")

model = LogisticRegression()
if 'waffle_calories' in foodsentiment_df.columns:
    model.fit(X, y)
    predictions = model.predict(X)
    y_pred = model.predict(X)
    print(y_pred [:4])


# In[69]:


pip install caserecommender


# In[73]:


import pandas as pd
import numpy as np


# In[74]:


foodrecommend_df = pd.read_csv('foodchoicesample.csv')
foodrecommend_df.head()


# In[75]:


foodrecommend_df.Gender.value_counts().plot(kind='bar', color=['g', 'c', 'y', 'b', 'r']);


# In[76]:


foodrecommend_df.marital_status.value_counts().plot(kind='bar', color=['g', 'c', 'y', 'b', 'r']);


# In[77]:


foodrecommend_df.marital_status.value_counts().plot(kind='bar', color=['g', 'c', 'y', 'b', 'r']);


# In[84]:


foodrecommend_df.on_off_campus.value_counts().plot(kind='bar', color=['g', 'c', 'y', 'b', 'r']);


# In[86]:


foodrecommend_df.sports.value_counts().plot(kind='bar', color=['g', 'c', 'y', 'b', 'r']);


# In[88]:


foodrecommend_df_metadata = pd.read_csv('foodchoicesample.csv')
foodrecommend_df_metadata.head()


# In[90]:


df_foodrecommend = foodrecommend_df[['Gender', 'sports', 'on_off_campus']] 
df_foodrecommend.head()


# In[92]:


# unique users
df_foodrecommend.Gender.unique()


# In[94]:


# unique items
df_foodrecommend.sports.unique()


# In[96]:


df_foodrecommend.tail()


# In[98]:


map_gender = {gender: u_id for u_id, gender in enumerate(df_foodrecommend.Gender.unique())}
map_sports = {sports: i_id for i_id, sports in enumerate(df_foodrecommend.sports.unique())}


# In[100]:


sports = {
    0:  'car racing',
    1:  'Basketball',
    2:  'Softball',
}

df_foodrecommend['Gender'] = df_foodrecommend['Gender'].map(map_gender)
df_foodrecommend['sports'] = df_foodrecommend['sports'].map(sports)


# In[102]:


df_foodrecommend.head()


# In[106]:


fav_sports_gender = {}

for idx, row in df_foodrecommend.iterrows():
    fav_sports_gender[row['Gender']] = row['sports']
    
np.save('map_gender.npy', fav_sports_gender)


# In[110]:


from sklearn.model_selection import train_test_split

train, test = train_test_split(df_foodrecommend, test_size=0.33, random_state=42)
train.to_csv('gender_train.dat', index=False, header=False, sep='\t')
test.to_csv('gender_test.dat', index=False, header=False, sep='\t')


# In[114]:


pip install caserec


# In[130]:


from caserec.recommenders.rating_prediction.most_popular import MostPopular

def validate_data(file_path):
    # ... (same as before)

# ... (same as before)

    if validate_data(train_file_path) and validate_data(test_file_path):
            mp = MostPopular(train_file_path, test_file_path, output_file_path)
            print("Training the model...")
            mp.fit()
            print("Predicting ratings...")
            mp.predict()
            print("Saving recommendations...")
            mp.save_recommendation()

    print("Data validation failed. Please check your input files.")


# In[143]:


import pandas as pd

foodranking_df = pd.read_csv('gender_train.dat', sep='\t', names=['Gender', 'sports'])
foodranking_df['Gender'] = foodranking_df.Gender.map(fav_sports_gender)
foodranking_df.head(10)


# In[146]:


train[train.Gender == 0]


# In[ ]:




